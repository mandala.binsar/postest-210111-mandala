//IMPLEMENTATION USING ASYNC AWAIT
//Mandala Binsar Panungkunan
console.log("RUNNING IMPLEMENTATION USING ASYNC AWAIT");

const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const { Todo, User } = require("./models/");

app.use(express.json());

app.get("/", (req, res) => {
  res.status(200).json({
    status: true,
    message: "IMPLEMENTATION USING ASYNC AWAIT",
  });
});

//
//CRUD User
//

//get all user
app.get("/users", async (req, res) => {
  try {
    const users = await User.findAll();
    res.status(200).json({
      status: true,
      message: "User list retreived with async await",
      data: { users },
    });
  } catch (error) {
    res.json(error);
  }
});

//get user by id
app.get("/users/:id", async (req, res) => {
  try {
    const user = await User.findByPk(req.params.id);
    res.status(200).json({
      status: true,
      message: `User with ID ${req.params.id} retreived with async await`,
      data: { user },
    });
  } catch (error) {
    res.json(error);
  }
});

//create user
app.post("/users", async (req, res) => {
  try {
    const user = await User.create({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    });
    res.status(201).json({
      status: true,
      message: "User created with async await",
      data: user,
    });
  } catch (error) {
    res.json(error);
  }
});

//update user
app.put("/users/:id", async (req, res) => {
  try {
    const user = await User.update(
      {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
      },
      {
        where: {
          id: req.params.id,
        },
      }
    );

    const updatedUser = await User.findByPk(req.params.id);

    res.status(200).json({
      status: true,
      message: `User with ID ${req.params.id} updated with async await`,
      data: updatedUser,
    });
  } catch (error) {
    res.json(error);
  }
});

//delete user
app.delete("/users/:id", async (req, res) => {
  try {
    const user = await User.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json({
      status: true,
      message: `User with ID ${req.params.id} deleted with async await`,
    });
  } catch (error) {
    res.json(error);
  }
});

//
//CRUD Todo
//

//get all todo
app.get("/todos", async (req, res) => {
  try {
    const todos = await Todo.findAll();
    res.status(200).json({
      status: true,
      message: "Todo list retreived with async await",
      data: { todos },
    });
  } catch (error) {
    res.json(error);
  }
});

//get todo by id
app.get("/todos/:id", async (req, res) => {
  try {
    const todo = await Todo.findByPk(req.params.id);
    res.status(200).json({
      status: true,
      message: `Todo with ID ${req.params.id} retreived with async await`,
      data: { todo },
    });
  } catch (error) {
    res.json(error);
  }
});

//create todo
app.post("/todos", async (req, res) => {
  try {
    const todo = await Todo.create({
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id: req.body.user_id,
    });
    res.status(201).json({
      status: true,
      message: "Todo created with async await",
      data: todo,
    });
  } catch (error) {
    res.json(error);
  }
});

//update todo
app.put("/todos/:id", async (req, res) => {
  try {
    const todo = await Todo.update(
      {
        name: req.body.name,
        description: req.body.description,
        due_at: req.body.due_at,
        user_id: req.body.user_id,
      },
      {
        where: {
          id: req.params.id,
        },
      }
    );

    const updatedTodo = await Todo.findByPk(req.params.id);

    res.status(200).json({
      status: true,
      message: `Todo with ID ${req.params.id} updated with async await`,
      data: updatedTodo,
    });
  } catch (error) {
    res.json(error);
  }
});

//delete todo
app.delete("/todos/:id", async (req, res) => {
  try {
    const todo = await Todo.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json({
      status: true,
      message: `Todo with ID ${req.params.id} deleted with async await`,
    });
  } catch (error) {
    res.json(error);
  }
});

app.listen(port, () =>
  console.log(
    `Listening on port ${port}! listening at http://localhost:${port}`
  )
);
