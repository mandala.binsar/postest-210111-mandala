//IMPLEMENTATION USING PROMISE
//Mandala Binsar Panungkunan
console.log("RUNNING IMPLEMENTATION USING PROMISE");

const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const { Todo, User } = require("./models/");

app.use(express.json());

app.get("/", (req, res) => {
  res.status(200).json({
    status: true,
    message: "IMPLEMENTATION USING PROMISE",
  });
});

//
//CRUD User
//

//get all user
app.get("/users", (req, res) => {
  User.findAll().then((users) => {
    res
      .status(200)
      .json({
        status: true,
        message: "User list retreived with promise",
        data: { users },
      })
      .catch((error) => res.json(error));
  });
});

//get user by ID
app.get("/users/:id", (req, res) => {
  User.findByPk(req.params.id)
    .then((user) => {
      res.status(200).json({
        status: true,
        message: `User with ID ${req.params.id} retrieved with promise`,
        data: user,
      });
    })
    .catch((error) => res.json(error));
});

//create user
app.post("/users", (req, res) => {
  User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  })
    .then((user) => {
      res.status(201).json({
        status: true,
        message: "User created with promise",
        data: user,
      });
    })
    .catch((error) => res.json(error));
});

//update user
app.put("/users/:id", (req, res) => {
  User.findByPk(req.params.id).then((user) => {
    user
      .update(
        {
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then(() => {
        res.status(200).json({
          status: true,
          message: `User with ID ${req.params.id} updated with promise`,
          data: user,
        });
      })
      .catch((error) => res.json(error));
  });
});

//delete user
app.delete("/users/:id", (req, res) => {
  User.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => {
      res.status(200).json({
        status: true,
        message: `User with ID ${req.params.id} deleted with promise`,
      });
    })
    .catch((error) => res.json(error));
});

//
//CRUD Todo
//

//get all todo
app.get("/todos", (req, res) => {
  Todo.findAll()
    .then((todos) => {
      res.status(200).json({
        status: true,
        message: "Todo list retreived with promise",
        data: { todos },
      });
    })
    .catch((error) => res.json(error));
});

//get todo by ID
app.get("/todos/:id", (req, res) => {
  Todo.findByPk(req.params.id)
    .then((todo) => {
      res.status(200).json({
        status: true,
        message: `Todo with ID ${req.params.id} retrieved with promise`,
        data: todo,
      });
    })
    .catch((error) => res.json(error));
});

//create todo
app.post("/todos", (req, res) => {
  Todo.create({
    name: req.body.name,
    description: req.body.description,
    due_at: req.body.due_at,
    user_id: req.body.user_id,
  })
    .then((todo) => {
      res.status(201).json({
        status: true,
        message: "Todo created with promise",
        data: todo,
      });
    })
    .catch((error) => res.json(error));
});

//update todo
app.put("/todos/:id", (req, res) => {
  Todo.findByPk(req.params.id)
    .then((todo) => {
      todo
        .update(
          {
            name: req.body.name,
            description: req.body.description,
            due_at: req.body.due_at,
            user_id: req.body.user_id,
          },
          {
            where: {
              id: req.params.id,
            },
          }
        )
        .then(() => {
          res.status(200).json({
            status: true,
            message: `Todo with ID ${req.params.id} updated with promise`,
            data: todo,
          });
        });
    })
    .catch((error) => res.json(error));
});

//delete todo
app.delete("/todos/:id", (req, res) => {
  Todo.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => {
      res.status(200).json({
        status: true,
        message: `Todo with ID ${req.params.id} deleted with promise`,
      });
    })
    .catch((error) => res.json(error));
});

app.listen(port, () =>
  console.log(
    `Listening on port ${port}! listening at http://localhost:${port}`
  )
);
