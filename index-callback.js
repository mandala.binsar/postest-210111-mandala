//IMPLEMENTATION USING CALLBACK
//Mandala Binsar Panungkunan
console.log("RUNNING IMPLEMENTATION USING CALLBACK");

const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const { Todo, User } = require("./models/");

app.use(express.json());

app.get("/", (req, res) => {
  res.status(200).json({
    status: true,
    message: "IMPLEMENTATION USING CALLBACK",
  });
});

//
//CRUD User
//

//Get All User
app.get("/users", (req, res) => {
  getUsers((error, users) => {
    if (error) {
      res.json(error);
    }
    res.status(200).json({
      status: true,
      message: "User list retreived with callback",
      data: { users },
    });
  });
});

//Get User By Id
app.get("/users/:id", (req, res) => {
  getUserById(req.params.id, (error, user) => {
    if (error) {
      res.json(error);
    }
    res.status(200).json({
      status: true,
      message: `User with ID ${req.params.id} retreived with callback`,
      data: { user },
    });
  });
});

//Create User
app.post("/users", (req, res) => {
  createUser(
    {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    },
    (error, user) => {
      if (error) {
        res.json(error);
      }
      res.status(200).json({
        status: true,
        message: "User created with with callback",
        data: { user },
      });
    }
  );
});

//Update User
app.put("/users/:id", (req, res) => {
  updateUser(
    req.params.id,
    { name: req.body.name, email: req.body.email, password: req.body.password },
    (error, user) => {
      if (error) {
        res.json(error);
      }
      res.status(200).json({
        status: true,
        message: `User with ID ${req.params.id} updated with callback`,
        data: { user },
      });
    }
  );
});

//Delete User
app.delete("/users/:id", (req, res) => {
  deleteUser(req.params.id, (error) => {
    if (error) {
      res.json.error;
    }
    res.status(200).json({
      status: true,
      message: `User with ID ${req.params.id} deleted with callback`,
    });
  });
});

//CRUD Todo

//Get All Todo
app.get("/todos", (req, res) => {
  getTodos((error, todos) => {
    if (error) {
      res.json(error);
    }
    res.status(200).json({
      status: true,
      message: "Todo list retreived with callback",
      data: { todos },
    });
  });
});

//Get Todo By Id
app.get("/todos/:id", (req, res) => {
  getTodoById(req.params.id, (error, todo) => {
    if (error) {
      res.json(error);
    }
    res.status(200).json({
      status: true,
      message: `Todo with ID ${req.params.id} retreived with callback`,
      data: { todo },
    });
  });
});

//Create Todo
app.post("/todos", (req, res) => {
  createTodo(
    {
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id: req.body.user_id,
    },
    (error, todo) => {
      if (error) {
        res.json(error);
      }
      res.status(200).json({
        status: true,
        message: "Todo created with with callback",
        data: { todo },
      });
    }
  );
});

//Update Todo
app.put("/todos/:id", (req, res) => {
  updateTodo(
    req.params.id,
    {
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id: req.body.user_id,
    },
    (error, todo) => {
      if (error) {
        res.json(error);
      }
      res.status(200).json({
        status: true,
        message: `Todo with ID ${req.params.id} updated with callback`,
        data: { todo },
      });
    }
  );
});

//Delete Todo
app.delete("/todos/:id", (req, res) => {
  deleteTodo(req.params.id, (error) => {
    if (error) {
      res.json.error;
    }
    res.status(200).json({
      status: true,
      message: `Todo with ID ${req.params.id} deleted with callback`,
    });
  });
});

app.listen(port, () =>
  console.log(
    `Listening on port ${port}! listening at http://localhost:${port}`
  )
);

//
//Function for User CRUD
//
const getUsers = function (callback) {
  User.findAll()
    .then((users) => {
      return callback(null, users);
    })
    .catch((error) => {
      return callback(error, null);
    });
};

const getUserById = function (id, callback) {
  User.findByPk(id)
    .then((users) => {
      return callback(null, users);
    })
    .catch((error) => {
      return callback(error, null);
    });
};

const createUser = function (user, callback) {
  User.create(user)
    .then((user) => {
      return callback(null, user);
    })
    .catch((error) => {
      return callback(error, null);
    });
};

const updateUser = function (updateID, updateParam, callback) {
  User.findByPk(updateID).then((user) => {
    user
      .update(
        {
          name: updateParam.name,
          email: updateParam.email,
          password: updateParam.password,
        },
        { where: { id: updateID } }
      )
      .then((updatedUser) => {
        return callback(null, updatedUser);
      })
      .catch((error) => {
        return callback(error, null);
      });
  });
};

const deleteUser = function (id, callback) {
  User.destroy({
    where: {
      id: id,
    },
  })
    .then(() => {
      return callback(null);
    })
    .catch((error) => {
      return callback(error);
    });
};

//
//Function for Todo CRUD
//
const getTodos = function (callback) {
  Todo.findAll()
    .then((todos) => {
      return callback(null, todos);
    })
    .catch((error) => {
      return callback(error, null);
    });
};

const getTodoById = function (id, callback) {
  Todo.findByPk(id)
    .then((todo) => {
      return callback(null, todo);
    })
    .catch((error) => {
      return callback(error, null);
    });
};

const createTodo = function (todo, callback) {
  Todo.create(todo)
    .then((todo) => {
      return callback(null, todo);
    })
    .catch((error) => {
      return callback(error, null);
    });
};

const updateTodo = function (updateID, updateParam, callback) {
  Todo.findByPk(updateID).then((todo) => {
    todo
      .update(
        {
          name: updateParam.name,
          description: updateParam.description,
          due_at: updateParam.due_at,
          user_id: updateParam.user_id,
        },
        { where: { id: updateID } }
      )
      .then((updatedTodo) => {
        return callback(null, updatedTodo);
      })
      .catch((error) => {
        return callback(error, null);
      });
  });
};

const deleteTodo = function (id, callback) {
  Todo.destroy({
    where: {
      id: id,
    },
  })
    .then(() => {
      return callback(null);
    })
    .catch((error) => {
      return callback(error);
    });
};

//Graveyard
//
//
//
//
//
//
//
//

/*
const updateUser = function (id, user, callback) {
  User.update(user, { where: { id } })
    .then((updatedUser) => {
      return callback(null, updatedUser);
    })
    .catch((error) => {
      return callback(error, null);
    });
};
*/

/*
app.put("/users/:id", (req, res) => {
  User.findByPk(req.params.id).then((user) => {
    user
      .update(
        {
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then(() => {
        res.status(200).json({
          status: true,
          message: `Users with ID ${req.params.id} updated!`,
          data: user,
        });
      });
  });
});
*/
